package com.naga.share.market.fundamental.analysis.services.cagr;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.naga.share.market.fundamental.analysis.model.cagr.entity.CompoundedAnnualGrowthRate;
import com.naga.share.market.fundamental.analysis.model.cagr.response.CagrAnalyzedDetails;
import com.naga.share.market.fundamental.analysis.model.cagr.response.CagrResponseDTO;
import com.naga.share.market.fundamental.analysis.model.company.entity.Company;
import com.naga.share.market.fundamental.analysis.model.company.response.CompanyResponseDTO;
import com.naga.share.market.fundamental.analysis.model.industry.response.IndustryResponseDTO;
import com.naga.share.market.fundamental.analysis.model.sector.response.SectorResponseDTO;
import com.naga.share.market.fundamental.analysis.services.constants.CagrConstants;

/**
 * @author Nagaaswin S
 *
 */
@Service
public class CagrDataAssignmentServiceImpl implements CagrDataAssignmentService {

	Logger logger = LogManager.getLogger(this.getClass());

	@Override
	public CagrResponseDTO assignBasicDetailsResponse(Map<String, Object> inputParams) {
		logger.info("CagrDataAssignmentServiceImpl assignBasicDetailsResponse method -start");
		CompoundedAnnualGrowthRate cagrAnalyzedDetails = (CompoundedAnnualGrowthRate) inputParams
				.get(CagrConstants.CAGR_ANALYZED_DETAILS);
		CagrResponseDTO cagrResponse = (CagrResponseDTO) inputParams.get(CagrConstants.CAGR_ANALYZED_RESPONSE);
		Company company = (Company) inputParams.get(CagrConstants.COMPANY);
		SectorResponseDTO sectorResponse = new SectorResponseDTO();
		IndustryResponseDTO industryResponse = new IndustryResponseDTO();
		CompanyResponseDTO companyResponse = new CompanyResponseDTO();
		CagrAnalyzedDetails cagrAnalyzedDetailsResponse = new CagrAnalyzedDetails();
		sectorResponse.setSectorName(company.getSector().getSectorName());
		industryResponse.setIndustryName(company.getIndustry().getIndustryName());
		companyResponse.setCompanyName(company.getCompanyName());
		companyResponse.setCompanySymbol(company.getCompanySymbol());
		cagrResponse.setSector(sectorResponse);
		cagrResponse.setIndustry(industryResponse);
		cagrResponse.setCompany(companyResponse);
		cagrAnalyzedDetailsResponse.setCagrStartYear(cagrAnalyzedDetails.getCompoundedAnnualGrowthRateStartYear());
		cagrAnalyzedDetailsResponse.setCagrEndYear(cagrAnalyzedDetails.getCompoundedAnnualGrowthRateEndYear());
		cagrAnalyzedDetailsResponse.setNetFreeCashflow(cagrAnalyzedDetails.getNetFreeCashflow());
		cagrAnalyzedDetailsResponse.setAverageFreeCashFlow(cagrAnalyzedDetails.getAverageFreeCashFlow());
		cagrAnalyzedDetailsResponse.setTotalRevenue(cagrAnalyzedDetails.getTotalRevenue());
		cagrAnalyzedDetailsResponse.setNetSales(cagrAnalyzedDetails.getNetSales());
		cagrAnalyzedDetailsResponse.setFinanceCost(cagrAnalyzedDetails.getFinanceCost());
		cagrAnalyzedDetailsResponse.setSharePrice(cagrAnalyzedDetails.getSharePrice());
		cagrAnalyzedDetailsResponse.setShareholdersFund(cagrAnalyzedDetails.getShareholdersFund());
		cagrAnalyzedDetailsResponse.setInventory(cagrAnalyzedDetails.getInventory());
		cagrAnalyzedDetailsResponse.setTradeReceivables(cagrAnalyzedDetails.getTradeReceivables());
		cagrAnalyzedDetailsResponse
				.setCashFromOperatingActivities(cagrAnalyzedDetails.getCashFromOperatingActivities());
		cagrAnalyzedDetailsResponse.setCashEquivalentAtYearEnding(cagrAnalyzedDetails.getCashEquivalentAtYearEnding());
		cagrAnalyzedDetailsResponse.setTotalDebt(cagrAnalyzedDetails.getTotalDebt());
		cagrAnalyzedDetailsResponse.setOperatingRevenue(cagrAnalyzedDetails.getOperatingRevenue());
		cagrAnalyzedDetailsResponse.setGrossProfit(cagrAnalyzedDetails.getGrossProfit());
		cagrAnalyzedDetailsResponse
				.setEarningsBfrIncomeTaxDepriAndAmor(cagrAnalyzedDetails.getEarningsBfrIncomeTaxDepriAndAmor());
		cagrAnalyzedDetailsResponse.setEarningsBeforeIncomeTax(cagrAnalyzedDetails.getEarningsBeforeIncomeTax());
		cagrAnalyzedDetailsResponse.setProfitAfterTax(cagrAnalyzedDetails.getProfitAfterTax());
		cagrAnalyzedDetailsResponse.setSalesPerShare(cagrAnalyzedDetails.getSalesPerShare());
		cagrAnalyzedDetailsResponse.setBookValuePerShare(cagrAnalyzedDetails.getBookValuePerShare());
		cagrAnalyzedDetailsResponse.setEarningsPerShare(cagrAnalyzedDetails.getEarningsPerShare());
		cagrResponse.setCagrAnalyzedDetails(cagrAnalyzedDetailsResponse);
		logger.info("CagrDataAssignmentServiceImpl assignBasicDetailsResponse method -end");
		return cagrResponse;
	}

	@Override
	public CagrResponseDTO assignGrowthAndHistoricalDataResponse(Map<String, Object> inputParams) {
		logger.info("CagrDataAssignmentServiceImpl assignGrowthAndHistoricalDataResponse method -start");
		CompoundedAnnualGrowthRate cagrAnalyzedDetails = (CompoundedAnnualGrowthRate) inputParams
				.get(CagrConstants.CAGR_ANALYZED_DETAILS);
		CagrResponseDTO cagrResponse = (CagrResponseDTO) inputParams.get(CagrConstants.CAGR_ANALYZED_RESPONSE);
		CagrAnalyzedDetails cagrAnalyzedDetailsResponse = cagrResponse.getCagrAnalyzedDetails();
		cagrAnalyzedDetailsResponse
				.setHistoricalTotalRevenue(cagrAnalyzedDetails.getHistoricalTotalRevenue().getHistoricalData());
		cagrAnalyzedDetailsResponse
				.setTotalRevenueGrowth(cagrAnalyzedDetails.getTotalRevenueGrowth().getHistoricalData());
		cagrAnalyzedDetailsResponse
				.setHistoricalProfitAfterTax(cagrAnalyzedDetails.getHistoricalProfitAfterTax().getHistoricalData());
		cagrAnalyzedDetailsResponse.setHistoricalProfitAfterTaxMargin(
				cagrAnalyzedDetails.getHistoricalProfitAfterTaxMargin().getHistoricalData());
		cagrAnalyzedDetailsResponse
				.setProfitAfterTaxGrowth(cagrAnalyzedDetails.getProfitAfterTaxGrowth().getHistoricalData());
		cagrAnalyzedDetailsResponse
				.setHistoricalSalesPerShare(cagrAnalyzedDetails.getHistoricalSalesPerShare().getHistoricalData());
		cagrAnalyzedDetailsResponse
				.setSalesPerShareGrowth(cagrAnalyzedDetails.getSalesPerShareGrowth().getHistoricalData());
		cagrAnalyzedDetailsResponse.setHistoricalBookValuePerShare(
				cagrAnalyzedDetails.getHistoricalBookValuePerShare().getHistoricalData());
		cagrAnalyzedDetailsResponse
				.setBookValuePerShareGrowth(cagrAnalyzedDetails.getBookValuePerShareGrowth().getHistoricalData());
		cagrAnalyzedDetailsResponse
				.setHistoricalEarningsPerShare(cagrAnalyzedDetails.getHistoricalEarningsPerShare().getHistoricalData());
		cagrAnalyzedDetailsResponse
				.setEarningsPerShareGrowth(cagrAnalyzedDetails.getEarningsPerShareGrowth().getHistoricalData());
		cagrAnalyzedDetailsResponse
				.setHistoricalGrossProfit(cagrAnalyzedDetails.getHistoricalGrossProfit().getHistoricalData());
		cagrAnalyzedDetailsResponse.setHistoricalGrossProfitMargin(
				cagrAnalyzedDetails.getHistoricalGrossProfitMargin().getHistoricalData());
		cagrAnalyzedDetailsResponse
				.setGrossProfitGrowth(cagrAnalyzedDetails.getGrossProfitGrowth().getHistoricalData());
		cagrAnalyzedDetailsResponse
				.setHistoricalTotalDebt(cagrAnalyzedDetails.getHistoricalTotalDebt().getHistoricalData());
		cagrAnalyzedDetailsResponse.setHistoricalEarningsBeforeIncTax(
				cagrAnalyzedDetails.getHistoricalEarningsBeforeIncTax().getHistoricalData());
		cagrAnalyzedDetailsResponse.setHistoricalTotalDebtByEarningsBfrIncTax(
				cagrAnalyzedDetails.getHistoricalTotalDebtByEarningsBfrIncTax().getHistoricalData());
		cagrAnalyzedDetailsResponse
				.setHistoricalInventory(cagrAnalyzedDetails.getHistoricalInventory().getHistoricalData());
		cagrAnalyzedDetailsResponse.setInventoryGrowth(cagrAnalyzedDetails.getInventoryGrowth().getHistoricalData());
		cagrAnalyzedDetailsResponse.setHistoricalInventoryNoOfDays(
				cagrAnalyzedDetails.getHistoricalInventoryNoOfDays().getHistoricalData());
		cagrAnalyzedDetailsResponse
				.setHistoricalNetSales(cagrAnalyzedDetails.getHistoricalNetSales().getHistoricalData());
		cagrAnalyzedDetailsResponse.setNetSalesGrowth(cagrAnalyzedDetails.getNetSalesGrowth().getHistoricalData());
		cagrAnalyzedDetailsResponse
				.setHistoricalTradeReceivables(cagrAnalyzedDetails.getHistoricalTradeReceivables().getHistoricalData());
		cagrAnalyzedDetailsResponse.setHistoricalTradeRecievablesByNetSales(
				cagrAnalyzedDetails.getHistoricalTradeRecievablesByNetSales().getHistoricalData());
		cagrAnalyzedDetailsResponse.setHistoricalCashFlowFromOperations(
				cagrAnalyzedDetails.getHistoricalCashFlowFromOperations().getHistoricalData());
		cagrAnalyzedDetailsResponse.setCashFlowFromOperationsGrowth(
				cagrAnalyzedDetails.getCashFlowFromOperationsGrowth().getHistoricalData());
		cagrAnalyzedDetailsResponse.setHistoricalCashAndCashEquivalent(
				cagrAnalyzedDetails.getHistoricalCashAndCashEquivalent().getHistoricalData());
		cagrAnalyzedDetailsResponse.setCashAndCashEquivalentGrowth(
				cagrAnalyzedDetails.getCashAndCashEquivalentGrowth().getHistoricalData());
		cagrAnalyzedDetailsResponse
				.setHistoricalShareholdersFund(cagrAnalyzedDetails.getHistoricalShareholdersFund().getHistoricalData());
		cagrAnalyzedDetailsResponse
				.setShareholdersFundGrowth(cagrAnalyzedDetails.getShareholdersFundGrowth().getHistoricalData());
		cagrAnalyzedDetailsResponse
				.setHistoricalReturnOnEquity(cagrAnalyzedDetails.getHistoricalReturnOnEquity().getHistoricalData());
		cagrAnalyzedDetailsResponse
				.setHistoricalOperatingRevenue(cagrAnalyzedDetails.getHistoricalOperatingRevenue().getHistoricalData());
		cagrAnalyzedDetailsResponse
				.setHistoricalSharePrice(cagrAnalyzedDetails.getHistoricalSharePrice().getHistoricalData());
		cagrAnalyzedDetailsResponse.setSharePriceGrowth(cagrAnalyzedDetails.getSharePriceGrowth().getHistoricalData());
		logger.info("CagrDataAssignmentServiceImpl assignGrowthAndHistoricalDataResponse method -end");
		return cagrResponse;
	}

	@Override
	public CagrResponseDTO assignCashFlowAndChecksResponse(Map<String, Object> inputParams) {
		logger.info("CagrDataAssignmentServiceImpl assignCashFlowAndChecksResponse method -start");
		CompoundedAnnualGrowthRate cagrAnalyzedDetails = (CompoundedAnnualGrowthRate) inputParams
				.get(CagrConstants.CAGR_ANALYZED_DETAILS);
		CagrResponseDTO cagrResponse = (CagrResponseDTO) inputParams.get(CagrConstants.CAGR_ANALYZED_RESPONSE);
		CagrAnalyzedDetails cagrAnalyzedDetailsResponse = cagrResponse.getCagrAnalyzedDetails();
		cagrAnalyzedDetailsResponse
				.setFutureCashFlowValue(cagrAnalyzedDetails.getFutureCashFlowValue().getHistoricalData());
		cagrAnalyzedDetailsResponse.setTerminalValue(cagrAnalyzedDetails.getTerminalValue());
		cagrAnalyzedDetailsResponse
				.setPresentFutureCashFlowValue(cagrAnalyzedDetails.getPresentFutureCashFlowValue().getHistoricalData());
		cagrAnalyzedDetailsResponse
				.setPresentValueOfTerminalValue(cagrAnalyzedDetails.getPresentValueOfTerminalValue());
		cagrAnalyzedDetailsResponse.setNetDebt(cagrAnalyzedDetails.getNetDebt());
		cagrAnalyzedDetailsResponse.setNetPresentValue(cagrAnalyzedDetails.getNetPresentValue());
		cagrAnalyzedDetailsResponse
				.setTotalPresentValueOfFreeFlowCash(cagrAnalyzedDetails.getTotalPresentValueOfFreeFlowCash());
		cagrAnalyzedDetailsResponse.setInternsicValueOfSharePrice(cagrAnalyzedDetails.getInternsicValueOfSharePrice());
		cagrAnalyzedDetailsResponse
				.setLowerInternsicValueOfSharePrice(cagrAnalyzedDetails.getLowerInternsicValueOfSharePrice());
		cagrAnalyzedDetailsResponse
				.setUpperInternsicValueOfSharePrice(cagrAnalyzedDetails.getUpperInternsicValueOfSharePrice());
		cagrAnalyzedDetailsResponse.setMarginOfSafety(cagrAnalyzedDetails.getMarginOfSafety());
		cagrAnalyzedDetailsResponse.setTotalRevenueInLineWithProfitAfterTaxCheck(
				cagrAnalyzedDetails.getTotalRevenueInLineWithProfitAfterTaxCheck());
		cagrAnalyzedDetailsResponse.setEarningsPerShareGrowthInLineWithProfitAfterTaxCheck(
				cagrAnalyzedDetails.getEarningsPerShareGrowthInLineWithProfitAfterTaxCheck());
		cagrAnalyzedDetailsResponse.setGrossProfitMarginCheck(cagrAnalyzedDetails.getGrossProfitMarginCheck());
		cagrAnalyzedDetailsResponse.setDebtLevelCheck(cagrAnalyzedDetails.getDebtLevelCheck());
		cagrAnalyzedDetailsResponse.setInventoryGrowthInLineWithProfitAfterTaxCheck(
				cagrAnalyzedDetails.getInventoryGrowthInLineWithProfitAfterTaxCheck());
		cagrAnalyzedDetailsResponse
				.setTradeReceivablesToNetSalesCheck(cagrAnalyzedDetails.getTradeReceivablesToNetSalesCheck());
		cagrAnalyzedDetailsResponse
				.setCashFlowFromOperationsCheck(cagrAnalyzedDetails.getCashFlowFromOperationsCheck());
		cagrAnalyzedDetailsResponse.setReturnOnEquityCheck(cagrAnalyzedDetails.getReturnOnEquityCheck());
		logger.info("CagrDataAssignmentServiceImpl assignCashFlowAndChecksResponse method -end");
		return cagrResponse;

	}

}
