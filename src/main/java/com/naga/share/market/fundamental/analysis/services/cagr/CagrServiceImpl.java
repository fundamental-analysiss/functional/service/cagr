package com.naga.share.market.fundamental.analysis.services.cagr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.naga.share.market.fundamental.analysis.adaptor.annual.AnnualDAO;
import com.naga.share.market.fundamental.analysis.adaptor.cagr.CompoundedAnnualGrowthRateDAO;
import com.naga.share.market.fundamental.analysis.adaptor.company.CompanyDAO;
import com.naga.share.market.fundamental.analysis.exception.InvalidDataException;
import com.naga.share.market.fundamental.analysis.exception.NoDataException;
import com.naga.share.market.fundamental.analysis.exception.constants.ExceptionsConstants;
import com.naga.share.market.fundamental.analysis.model.annual.entity.Annual;
import com.naga.share.market.fundamental.analysis.model.cagr.entity.CompoundedAnnualGrowthRate;
import com.naga.share.market.fundamental.analysis.model.cagr.request.CagrRequestDTO;
import com.naga.share.market.fundamental.analysis.model.cagr.response.CagrAnalyzedYears;
import com.naga.share.market.fundamental.analysis.model.cagr.response.CagrResponseDTO;
import com.naga.share.market.fundamental.analysis.model.company.entity.Company;
import com.naga.share.market.fundamental.analysis.model.company.response.CompanyResponseDTO;
import com.naga.share.market.fundamental.analysis.services.cagr.utils.CagrUtils;
import com.naga.share.market.fundamental.analysis.services.constants.CagrConstants;
import com.naga.share.market.fundamental.analysis.utils.ValidationUtils;

/**
 * @author Nagaaswin S
 *
 */
@Service
public class CagrServiceImpl implements CagrService {

	Logger logger = LogManager.getLogger(this.getClass());

	@Autowired
	CompanyDAO companyDAO;

	@Autowired
	AnnualDAO annualDAO;

	@Autowired
	CompoundedAnnualGrowthRateDAO cagrDAO;

	@Autowired
	CagrDataAssignmentService cagrDataAssignmentService;

	@Autowired
	CagrCalculationService cagrCalculationService;

	@Override
	public CagrResponseDTO analyzeCAGR(CagrRequestDTO cagrRequest, String analyzerId)
			throws InvalidDataException, NoDataException {
		logger.info("CagrServiceImpl - analyzeCAGR method - start");
		CagrResponseDTO cagrResponse = new CagrResponseDTO();
		Map<String, Object> inputParams = new HashMap<>();
		int cagrAnalysisStartYear = cagrRequest.getAnalysisStartYear();
		int cagrAnalysisEndYear = cagrRequest.getAnalysisEndYear();
		Company company = companyDAO.findByCompanyName(cagrRequest.getCompanyName());
		if (ValidationUtils.isNotNull(company)) {
			List<Annual> annuallyAnalyzedYears = annualDAO.findAllAnnuallyAnalyzedYearsForACompany(company);
			if (ValidationUtils.isNotNullAndEmpty(annuallyAnalyzedYears)) {
				List<Integer> analysisYears = new ArrayList<Integer>();
				for (Annual annual : annuallyAnalyzedYears) {
					analysisYears.add(annual.getAnalysisYear());
				}
				inputParams.put(CagrConstants.CAGR_ANALYSIS_START_YEAR, cagrAnalysisStartYear);
				inputParams.put(CagrConstants.CAGR_ANALYSIS_END_YEAR, cagrAnalysisEndYear);
				inputParams.put(CagrConstants.ANNUAL_ANALYSIS_YEARS, analysisYears);
				if (CagrUtils.validateStartAndEndYear(inputParams)) {
					boolean isPresent = false;
					List<Annual> annuallyAnalyzedDetails = annualDAO.findAnnuallyAnalyzedDetailsForACompanyBtwYears(
							company, cagrAnalysisStartYear, cagrAnalysisEndYear);
					CompoundedAnnualGrowthRate cagrAnalyzedDetails = cagrDAO.findByCompanyNameAndCAGRYears(company,
							cagrAnalysisStartYear, cagrAnalysisEndYear);
					if (ValidationUtils.isNotNull(cagrAnalyzedDetails)) {
						isPresent = true;
					} else {
						cagrAnalyzedDetails = new CompoundedAnnualGrowthRate();
					}
					inputParams.put(CagrConstants.COMPANY, company);
					inputParams.put(CagrConstants.IS_PRESENT, isPresent);
					inputParams.put(CagrConstants.CAGR_ANALYZED_DETAILS, cagrAnalyzedDetails);
					inputParams.put(CagrConstants.ANNUALLY_ANALYZED_DETAILS, annuallyAnalyzedDetails);
					inputParams.put(CagrConstants.CAGR_ANALYZED_RESPONSE, cagrResponse);
					cagrCalculationService.cagrCalculation(inputParams);
					if (isPresent) {
						cagrAnalyzedDetails.setLastUpdatedBy(analyzerId);
						cagrDAO.updateCAGRDetails(cagrAnalyzedDetails);
					} else {
						cagrAnalyzedDetails.setSector(company.getSector());
						cagrAnalyzedDetails.setIndustry(company.getIndustry());
						cagrAnalyzedDetails.setCompany(company);
						company.getSector().addCompoundedAnnualGrowthRate(cagrAnalyzedDetails);
						company.getIndustry().addCompoundedAnnualGrowthRate(cagrAnalyzedDetails);
						company.addCompoundedAnnualGrowthRate(cagrAnalyzedDetails);
						cagrAnalyzedDetails.setCreatedBy(analyzerId);
						cagrAnalyzedDetails.setLastUpdatedBy(analyzerId);
						cagrDAO.insertCAGRAnalyzedDetails(cagrAnalyzedDetails);
					}
					cagrDataAssignmentService.assignBasicDetailsResponse(inputParams);
					cagrDataAssignmentService.assignGrowthAndHistoricalDataResponse(inputParams);
					cagrDataAssignmentService.assignCashFlowAndChecksResponse(inputParams);
					logger.info("CagrServiceImpl - analyzeCAGR method - End");
					return cagrResponse;
				} else {
					logger.error("Invalid data!! for values startYear: {}, endYear:{}", cagrAnalysisStartYear,
							cagrAnalysisEndYear);
					throw new InvalidDataException(ExceptionsConstants.INVALID_DATA_ERROR);
				}
			} else {
				logger.error("No data analyzed for the company:{}", cagrRequest.getCompanyName());
				throw new NoDataException(ExceptionsConstants.NO_DATA_ERROR);
			}
		} else {
			logger.error("Invalid data CompanyName for value CompanyName:{}", cagrRequest.getCompanyName());
			throw new InvalidDataException(ExceptionsConstants.INVALID_DATA_ERROR);
		}
	}

	@Override
	public CagrResponseDTO retrieveAnalyzedData(String companyName, int cagrAnalysisStartYear, int cagrAnalysisEndYear)
			throws NoDataException, InvalidDataException {
		logger.info("CagrServiceImpl - retrieveAnalyzedData method - start");
		Company company = companyDAO.findByCompanyName(companyName);
		if (ValidationUtils.isNotNull(company)) {
			Map<String, Object> inputParams = new HashMap<>();
			CagrResponseDTO cagrResponse = new CagrResponseDTO();
			CompoundedAnnualGrowthRate cagrAnalyzedDetails = cagrDAO.findByCompanyNameAndCAGRYears(company,
					cagrAnalysisStartYear, cagrAnalysisEndYear);
			if (ValidationUtils.isNotNull(cagrAnalyzedDetails)) {
				inputParams.put(CagrConstants.COMPANY, company);
				inputParams.put(CagrConstants.CAGR_ANALYZED_RESPONSE, cagrResponse);
				inputParams.put(CagrConstants.CAGR_ANALYZED_DETAILS, cagrAnalyzedDetails);
				cagrDataAssignmentService.assignBasicDetailsResponse(inputParams);
				cagrDataAssignmentService.assignGrowthAndHistoricalDataResponse(inputParams);
				cagrDataAssignmentService.assignCashFlowAndChecksResponse(inputParams);
				logger.info("CagrServiceImpl - retrieveAnalyzedData method - end");
				return cagrResponse;
			} else {
				logger.error("CagrServiceImpl - retrieveAnalyzedData Method - CAGR analyzed details not found.");
				throw new NoDataException(ExceptionsConstants.NO_DATA_ERROR);
			}
		} else {
			logger.error("Invalid company Name : {}", companyName);
			throw new InvalidDataException(ExceptionsConstants.INVALID_DATA_ERROR,
					ExceptionsConstants.INVALID_DATA_ERROR);
		}
	}

	@Override
	public CagrResponseDTO retrieveAnalyzedYears(String companyName) throws NoDataException, InvalidDataException {
		logger.info("CagrServiceImpl - retrieveAnalyzedYears method - start");
		Company company = companyDAO.findByCompanyName(companyName);
		if (ValidationUtils.isNotNull(company)) {
			CagrResponseDTO companyCAGRAnalyzedYears = new CagrResponseDTO();
			CompanyResponseDTO companyResponse = new CompanyResponseDTO();
			companyResponse.setCompanyName(company.getCompanyName());
			companyResponse.setCompanySymbol(company.getCompanySymbol());
			companyCAGRAnalyzedYears.setCompany(companyResponse);
			List<CompoundedAnnualGrowthRate> cagrAnalyzedDetails = cagrDAO.findAllCAGRAnalyzedYearsForACompany(company);
			List<CagrAnalyzedYears> cagrYears = new ArrayList<>();
			for (CompoundedAnnualGrowthRate cagr : cagrAnalyzedDetails) {
				CagrAnalyzedYears year = new CagrAnalyzedYears();
				year.setCagrStartYear(cagr.getCompoundedAnnualGrowthRateStartYear());
				year.setCagrEndYear(cagr.getCompoundedAnnualGrowthRateEndYear());
				cagrYears.add(year);
			}
			companyCAGRAnalyzedYears.setCagrAnalyzedYears(cagrYears);
			logger.info("CagrServiceImpl - retrieveAnalyzedData method - end");
			return companyCAGRAnalyzedYears;
		} else {
			logger.error("Invalid company Name : {}", companyName);
			throw new InvalidDataException(ExceptionsConstants.INVALID_DATA_ERROR,
					ExceptionsConstants.INVALID_DATA_ERROR);
		}
	}

	@Override
	public void deleteAnalyzedData(String companyName, int cagrAnalysisStartYear, int cagrAnalysisEndYear)
			throws InvalidDataException, NoDataException {
		logger.info("AnnualServiceImpl - deleteAnalyzedData method - start");
		Company company = companyDAO.findByCompanyName(companyName);
		if (ValidationUtils.isNotNull(company)) {
			cagrDAO.deleteByCompanyNameAndCAGRYear(company, cagrAnalysisStartYear, cagrAnalysisEndYear);
		} else {
			logger.error("Invalid company Name : {}", companyName);
			throw new InvalidDataException(ExceptionsConstants.INVALID_DATA_ERROR,
					ExceptionsConstants.INVALID_DATA_ERROR);
		}
		logger.info("AnnualServiceImpl - deleteAnalyzedData method - end");
	}

}
