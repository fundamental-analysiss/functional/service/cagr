package com.naga.share.market.fundamental.analysis.services.cagr.utils;

import java.util.List;
import java.util.Map;

import com.naga.share.market.fundamental.analysis.model.cagr.HistoricalData;
import com.naga.share.market.fundamental.analysis.services.constants.CagrConstants;

/**
 * @author Nagaaswin S
 *
 */
public class CagrUtils {

	private CagrUtils() {
	}

	@SuppressWarnings("unchecked")
	public static boolean validateStartAndEndYear(Map<String, Object> inputParams) {
		int startYear = (int) inputParams.get(CagrConstants.CAGR_ANALYSIS_START_YEAR);
		int endYear = (int) inputParams.get(CagrConstants.CAGR_ANALYSIS_END_YEAR);
		List<Integer> analysisYears = (List<Integer>) inputParams.get(CagrConstants.ANNUAL_ANALYSIS_YEARS);
		if (endYear <= startYear) {
			return false;
		}
		for (int i = startYear; i <= endYear; i++) {
			if (!analysisYears.contains(i)) {
				return false;
			}
		}
		return true;
	}

	public static double compounding(double latestYearValue, double oldYearValue, double noOfYears) {
		double powerVal;
		double ratioVal;
		double yearRatio;
		ratioVal = latestYearValue / oldYearValue;
		yearRatio = 1 / noOfYears;
		powerVal = Math.pow(ratioVal, yearRatio);
		return ((powerVal - 1) * 100);
	}

	public static double futureCashFlowCalculation(double cashFlow, double annualInterest) {
		annualInterest = annualInterest / 100;
		return (cashFlow * (1 + annualInterest));
	}

	public static double terminalValueCalculation(double cashFlow, double terminalInterest, double discountRate) {
		discountRate = ((discountRate - terminalInterest) / 100);
		terminalInterest = cashFlow * (1 + (terminalInterest / 100));
		return (terminalInterest / discountRate);
	}

	public static double presentValueCalculation(double futureCashFlow, double discountRate, double futureYear) {
		double denominator = (1 + (discountRate / 100));
		denominator = Math.pow(denominator, futureYear);
		return (futureCashFlow / denominator);
	}

	public static double growthCalculation(double currentData, double priorYearData) {
		double numerator = currentData - priorYearData;
		return (numerator / priorYearData) * 100;
	}

	public static String totalRevenueInLineWithPATCheck(List<HistoricalData> totalRevenueGrowth,
			List<HistoricalData> ProfitAfterTaxGrowth) {
		int good = 0;
		int bad = 0;
		int average = 0;
		for (HistoricalData totalRevenue : totalRevenueGrowth) {
			for (HistoricalData profitAftertax : ProfitAfterTaxGrowth) {
				if (totalRevenue.getCurrentYear() == profitAftertax.getCurrentYear()
						&& totalRevenue.getPriorYear() == profitAftertax.getPriorYear()) {
					if (Math.abs(totalRevenue.getValue() - profitAftertax.getValue()) <= 10.00) {
						good++;
					} else if (Math.abs(totalRevenue.getValue() - profitAftertax.getValue()) <= 20.00) {
						average++;
					} else {
						bad++;
					}
					break;
				}
			}
		}
		return goodAverageBadCheck(good, average, bad);
	}

	public static String earningsPerShareInLineWithPATCheck(List<HistoricalData> earningsPerShareGrowth,
			List<HistoricalData> profitAfterTaxGrowth) {
		int good = 0;
		int bad = 0;
		int average = 0;
		for (HistoricalData earningsPerShare : earningsPerShareGrowth) {
			for (HistoricalData profitAftertax : profitAfterTaxGrowth) {
				if (earningsPerShare.getCurrentYear() == profitAftertax.getCurrentYear()
						&& earningsPerShare.getPriorYear() == profitAftertax.getPriorYear()) {
					if (Math.abs(earningsPerShare.getValue() - profitAftertax.getValue()) <= 10.00) {
						good++;
					} else if (Math.abs(earningsPerShare.getValue() - profitAftertax.getValue()) <= 20.00) {
						average++;
					} else {
						bad++;
					}
					break;
				}
			}
		}
		return goodAverageBadCheck(good, average, bad);
	}

	public static String grossProfitMariginCheck(List<HistoricalData> grossProfitMariginList) {
		int good = 0;
		int bad = 0;
		int average = 0;
		for (HistoricalData grossProfitMarigin : grossProfitMariginList) {
			if (grossProfitMarigin.getValue() > 20.00) {
				good++;
			} else if (grossProfitMarigin.getValue() <= 20.00 && grossProfitMarigin.getValue() > 15.00) {
				average++;
			} else {
				bad++;
			}
		}
		return goodAverageBadCheck(good, average, bad);
	}

	public static String debtLevelCheck(List<HistoricalData> totalDebtToEarningsBfrTax) {
		int good = 0;
		int bad = 0;
		int average = 0;
		for (int index = 0; index < totalDebtToEarningsBfrTax.size() - 1; index++) {
			if ((totalDebtToEarningsBfrTax.get(index + 1).getValue()
					- totalDebtToEarningsBfrTax.get(index).getValue()) < -5.00) {
				good++;
			} else if ((totalDebtToEarningsBfrTax.get(index + 1).getValue()
					- totalDebtToEarningsBfrTax.get(index).getValue()) < 0
					&& (totalDebtToEarningsBfrTax.get(index + 1).getValue()
							- totalDebtToEarningsBfrTax.get(index).getValue()) > -5.00) {
				average++;
			} else {
				bad++;
			}
		}
		return goodAverageBadCheck(good, average, bad);
	}

	public static String inventoryInLineWithPATCheck(List<HistoricalData> inventoryGrowth,
			List<HistoricalData> profitAfterTaxGrowth) {
		int good = 0;
		int bad = 0;
		int average = 0;
		for (HistoricalData inventory : inventoryGrowth) {
			for (HistoricalData profitAftertax : profitAfterTaxGrowth) {
				if (inventory.getCurrentYear() == profitAftertax.getCurrentYear()
						&& inventory.getPriorYear() == profitAftertax.getPriorYear()) {
					if (Math.abs(inventory.getValue() - profitAftertax.getValue()) <= 10.00) {
						good++;
					} else if (Math.abs(inventory.getValue() - profitAftertax.getValue()) <= 20.00) {
						average++;
					} else {
						bad++;
					}
					break;
				}
			}
		}
		return goodAverageBadCheck(good, average, bad);
	}

	public static String tradeRecievablesToNetSalesCheck(List<HistoricalData> tradeRecievablesToNetSales) {
		int good = 0;
		int bad = 0;
		int average = 0;
		for (int index = 0; index < tradeRecievablesToNetSales.size() - 1; index++) {
			if ((tradeRecievablesToNetSales.get(index).getValue()
					- tradeRecievablesToNetSales.get(index + 1).getValue()) > 1.00) {
				good++;
			} else if ((tradeRecievablesToNetSales.get(index).getValue()
					- tradeRecievablesToNetSales.get(index + 1).getValue()) > 0.00
					&& (tradeRecievablesToNetSales.get(index).getValue()
							- tradeRecievablesToNetSales.get(index + 1).getValue()) <= 1.00) {
				average++;
			} else {
				bad++;
			}
		}
		return goodAverageBadCheck(good, average, bad);
	}

	public static String cashFlowFromOperatingActivitiesCheck(
			List<HistoricalData> cashFlowFromOperatingActivitiesList) {
		int good = 0;
		int bad = 0;
		for (HistoricalData cashFlowFromOperatingActivities : cashFlowFromOperatingActivitiesList) {
			if (cashFlowFromOperatingActivities.getValue() > 0) {
				good++;
			} else {
				bad++;
			}
		}
		if (good > ((cashFlowFromOperatingActivitiesList.size()) / 2 + 1)) {
			return CagrConstants.GOOD;
		} else if (good >= bad) {
			return CagrConstants.AVERAGE;
		} else {
			return CagrConstants.BAD;
		}
	}

	public static String returnOnEquityCheck(List<HistoricalData> returnOnEquityList) {
		int good = 0;
		int bad = 0;
		int average = 0;
		for (HistoricalData returnOnEquity : returnOnEquityList) {
			if (returnOnEquity.getValue() > 20.00) {
				good++;
			} else if (returnOnEquity.getValue() <= 20.00 && returnOnEquity.getValue() >= 15.00) {
				average++;
			} else {
				bad++;
			}
		}
		return goodAverageBadCheck(good, average, bad);
	}

	private static String goodAverageBadCheck(int good, int average, int bad) {
		if ((good == average && good >= bad) || (good == bad) || (average > good && average > bad)) {
			return CagrConstants.AVERAGE;
		} else if (good > average && good > bad) {
			return CagrConstants.GOOD;
		} else {
			return CagrConstants.BAD;
		}

	}
}
