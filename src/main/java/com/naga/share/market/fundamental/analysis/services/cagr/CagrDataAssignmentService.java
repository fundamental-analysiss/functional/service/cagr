package com.naga.share.market.fundamental.analysis.services.cagr;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.naga.share.market.fundamental.analysis.model.cagr.response.CagrResponseDTO;

/**
 * @author Nagaaswin S
 *
 */
@Service
public interface CagrDataAssignmentService {

	/**
	 * Service method to assign basic details like Sector,Industry and Company to
	 * CAGR response
	 * 
	 * 
	 * @param inputParams
	 * @return CagrResponseDTO
	 */
	public CagrResponseDTO assignBasicDetailsResponse(Map<String, Object> inputParams);

	/**
	 * Service method to assign historical data of a company in CAGR response
	 * 
	 * @param inputParams
	 * @return CagrResponseDTO
	 */
	public CagrResponseDTO assignGrowthAndHistoricalDataResponse(Map<String, Object> inputParams);

	/**
	 * 
	 * Service method to assign CAGR analyzed data of a company in CAGR response
	 * 
	 * @param inputParams
	 * @return CagrResponseDTO
	 */
	public CagrResponseDTO assignCashFlowAndChecksResponse(Map<String, Object> inputParams);
}
