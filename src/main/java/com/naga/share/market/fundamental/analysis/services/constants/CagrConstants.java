package com.naga.share.market.fundamental.analysis.services.constants;

/**
 * @author Nagaaswin S
 *
 */
public class CagrConstants {

	private CagrConstants() {
	}

	public static final String CAGR_ANALYSIS_START_YEAR = "cagrAnalysisStartYear";
	public static final String CAGR_ANALYSIS_END_YEAR = "cagrAnalysisEndYear";
	public static final String COMPANY = "company";
	public static final String ANNUALLY_ANALYZED_DETAILS = "annuallyAnalyzedDetails";
	public static final String ANNUAL_ANALYSIS_YEARS = "annnualAnalysisYears";
	public static final String IS_PRESENT = "isPresent";
	public static final String CAGR_ANALYZED_DETAILS = "cagrAnalyzedDetails";
	public static final String CAGR_ANALYZED_RESPONSE = "cagrAnalyzedResponse";
	public static final String SINGLE_SPACE = " ";
	public static final double FUTURE_CASH_FLOW_FIRST_FIVE_YEARS_INTEREST = 17.00;
	public static final double FUTURE_CASH_FLOW_LAST_FIVE_YEARS_INTEREST = 10.00;
	public static final double TERMINAL_ANNUAL_INTEREST = 3.5;
	public static final double DISCOUNT_RATE = 9.0;
	public static final int BASE_VALUE = 100000;
	public static final double UPPER_INTERNSIC_VALUE_FACTOR = 1.1;
	public static final double LOWER_INTERNSIC_VALUE_FACTOR = 0.9;
	public static final double MARGIN_OF_SAFETY_FACTOR = 0.3;

	public static final String GOOD = "good";
	public static final String AVERAGE = "average";
	public static final String BAD = "bad";
}
