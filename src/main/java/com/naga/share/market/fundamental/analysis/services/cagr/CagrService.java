package com.naga.share.market.fundamental.analysis.services.cagr;

import org.springframework.stereotype.Service;

import com.naga.share.market.fundamental.analysis.exception.InvalidDataException;
import com.naga.share.market.fundamental.analysis.exception.NoDataException;
import com.naga.share.market.fundamental.analysis.model.cagr.request.CagrRequestDTO;
import com.naga.share.market.fundamental.analysis.model.cagr.response.CagrResponseDTO;

/**
 * @author Nagaaswin S
 *
 */
@Service
public interface CagrService {

	/**
	 * 
	 * Method to analyze to a company CAGR based on annually analyzed
	 * 
	 * @param cagrRequest
	 * @return CagrResponseDTO
	 * @throws InvalidDataException
	 * @throws NoDataException
	 */
	public CagrResponseDTO analyzeCAGR(CagrRequestDTO cagrRequest, String analyzerId)
			throws InvalidDataException, NoDataException;

	/**
	 * Method to retrieve a CAGR analyzed details of a company for a range of years
	 * 
	 * @param companyName
	 * @param cagrAnalysisStartYear
	 * @param cagrAnalysisEndYear
	 * @return CagrResponseDTO
	 * @throws NoDataException
	 * @throws InvalidDataException
	 */
	public CagrResponseDTO retrieveAnalyzedData(String companyName, int cagrAnalysisStartYear, int cagrAnalysisEndYear)
			throws NoDataException, InvalidDataException;

	/**
	 * 
	 * Method to retrieve a CAGR analyzed years of a company
	 * 
	 * @param companyName
	 * @return CagrResponseDTO
	 */
	public CagrResponseDTO retrieveAnalyzedYears(String companyName) throws NoDataException, InvalidDataException;

	/**
	 * 
	 * Method to delete a CAGR analyzed details of a company
	 * 
	 * @param companyName
	 * @param cagrAnalysisStartYear
	 * @param cagrAnalysisEndYear
	 */
	public void deleteAnalyzedData(String companyName, int cagrAnalysisStartYear, int cagrAnalysisEndYear)
			throws InvalidDataException, NoDataException;
}
