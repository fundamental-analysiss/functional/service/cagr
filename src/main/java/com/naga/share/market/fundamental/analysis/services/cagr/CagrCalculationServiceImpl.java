package com.naga.share.market.fundamental.analysis.services.cagr;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.naga.share.market.fundamental.analysis.model.annual.entity.Annual;
import com.naga.share.market.fundamental.analysis.model.cagr.HistoricalData;
import com.naga.share.market.fundamental.analysis.model.cagr.HistoricalDataWrapper;
import com.naga.share.market.fundamental.analysis.model.cagr.entity.CompoundedAnnualGrowthRate;
import com.naga.share.market.fundamental.analysis.services.cagr.utils.CagrUtils;
import com.naga.share.market.fundamental.analysis.services.constants.CagrConstants;

/**
 * @author Nagaaswin S
 *
 */
@Service
public class CagrCalculationServiceImpl implements CagrCalculationService {

	Logger logger = LogManager.getLogger(this.getClass());

	@Override
	public CompoundedAnnualGrowthRate cagrCalculation(Map<String, Object> inputParams) {
		logger.info("CagrCalculationServiceImpl CompoundedAnnualGrowthRate method -start");
		CompoundedAnnualGrowthRate cagrAnalyzedDetails = (CompoundedAnnualGrowthRate) inputParams
				.get(CagrConstants.CAGR_ANALYZED_DETAILS);
		cagrAnalyzedDetails
				.setCompoundedAnnualGrowthRateStartYear((int) inputParams.get(CagrConstants.CAGR_ANALYSIS_START_YEAR));
		cagrAnalyzedDetails
				.setCompoundedAnnualGrowthRateEndYear((int) inputParams.get(CagrConstants.CAGR_ANALYSIS_END_YEAR));
		cagrAnalyzedDetails = basicDetailsCalculation(inputParams);
		cagrAnalyzedDetails = futureCashFlowCalculation(inputParams);
		cagrAnalyzedDetails = presentValueCalculation(inputParams);
		cagrAnalyzedDetails = sharePriceCalculation(inputParams);
		cagrAnalyzedDetails = growthAndHistoricalDataCalculation(inputParams);
		cagrAnalyzedDetails = crucialDetailsCheckCalculation(inputParams);
		logger.info("CagrCalculationServiceImpl CompoundedAnnualGrowthRate method -end");
		return cagrAnalyzedDetails;
	}

	@SuppressWarnings("unchecked")
	@Override
	public CompoundedAnnualGrowthRate basicDetailsCalculation(Map<String, Object> inputParams) {
		logger.info("CagrCalculationServiceImpl basicDetailsCalculation method -start");
		CompoundedAnnualGrowthRate cagrAnalyzedDetails = (CompoundedAnnualGrowthRate) inputParams
				.get(CagrConstants.CAGR_ANALYZED_DETAILS);
		List<Annual> annuallyAnalyzedDetails = (List<Annual>) inputParams.get(CagrConstants.ANNUALLY_ANALYZED_DETAILS);
		Annual startYearAnnualData = annuallyAnalyzedDetails.get(0);
		Annual endYearAnnualData = annuallyAnalyzedDetails.get(annuallyAnalyzedDetails.size() - 1);
		int noOfYears = annuallyAnalyzedDetails.size();
		for (Annual annual : annuallyAnalyzedDetails) {
			cagrAnalyzedDetails.setNetFreeCashflow(cagrAnalyzedDetails.getNetFreeCashflow() + annual.getFreeCashFlow());
		}
		cagrAnalyzedDetails.setAverageFreeCashFlow(cagrAnalyzedDetails.getNetFreeCashflow() / noOfYears);
		cagrAnalyzedDetails.setTotalRevenue(CagrUtils.compounding(endYearAnnualData.getTotalRevenue(),
				startYearAnnualData.getTotalRevenue(), noOfYears));
		cagrAnalyzedDetails.setNetSales(
				CagrUtils.compounding(endYearAnnualData.getNetSales(), startYearAnnualData.getNetSales(), noOfYears));
		cagrAnalyzedDetails.setFinanceCost(CagrUtils.compounding(endYearAnnualData.getFinanceCost(),
				startYearAnnualData.getFinanceCost(), noOfYears));
		cagrAnalyzedDetails.setSharePrice(CagrUtils.compounding(endYearAnnualData.getSharePrice(),
				startYearAnnualData.getSharePrice(), noOfYears));
		cagrAnalyzedDetails
				.setShareholdersFund(CagrUtils.compounding(endYearAnnualData.getShareHoldersFundAndTotalEquity(),
						startYearAnnualData.getShareHoldersFundAndTotalEquity(), noOfYears));
		cagrAnalyzedDetails.setInventory(
				CagrUtils.compounding(endYearAnnualData.getInventory(), startYearAnnualData.getInventory(), noOfYears));
		cagrAnalyzedDetails.setTradeReceivables(CagrUtils.compounding(endYearAnnualData.getTradeReceivables(),
				startYearAnnualData.getTradeReceivables(), noOfYears));
		cagrAnalyzedDetails.setCashFromOperatingActivities(
				CagrUtils.compounding(endYearAnnualData.getCashFlowFromOperatingActivities(),
						startYearAnnualData.getCashFlowFromOperatingActivities(), noOfYears));
		cagrAnalyzedDetails.setCashEquivalentAtYearEnding(
				CagrUtils.compounding(endYearAnnualData.getCashAndCashEquivalentAtYearEnding(),
						startYearAnnualData.getCashAndCashEquivalentAtYearEnding(), noOfYears));
		cagrAnalyzedDetails.setTotalDebt(
				CagrUtils.compounding(endYearAnnualData.getTotalDebt(), startYearAnnualData.getTotalDebt(), noOfYears));
		cagrAnalyzedDetails.setOperatingRevenue(CagrUtils.compounding(endYearAnnualData.getOperatingRevenue(),
				startYearAnnualData.getOperatingRevenue(), noOfYears));
		cagrAnalyzedDetails.setGrossProfit(CagrUtils.compounding(endYearAnnualData.getGrossProfit(),
				startYearAnnualData.getGrossProfit(), noOfYears));
		cagrAnalyzedDetails.setEarningsBfrIncomeTaxDepriAndAmor(
				CagrUtils.compounding(endYearAnnualData.getEarningsBfrIncomeTaxDepriAndAmor(),
						startYearAnnualData.getEarningsBfrIncomeTaxDepriAndAmor(), noOfYears));
		cagrAnalyzedDetails
				.setEarningsBeforeIncomeTax(CagrUtils.compounding(endYearAnnualData.getEarningsBeforeIncomeTax(),
						startYearAnnualData.getEarningsBeforeIncomeTax(), noOfYears));
		cagrAnalyzedDetails.setProfitAfterTax(CagrUtils.compounding(endYearAnnualData.getProfitAfterTax(),
				startYearAnnualData.getProfitAfterTax(), noOfYears));
		cagrAnalyzedDetails.setSalesPerShare(CagrUtils.compounding(endYearAnnualData.getSalesPerShare(),
				startYearAnnualData.getSalesPerShare(), noOfYears));
		cagrAnalyzedDetails.setBookValuePerShare(CagrUtils.compounding(endYearAnnualData.getBookValuePerShare(),
				startYearAnnualData.getBookValuePerShare(), noOfYears));
		cagrAnalyzedDetails.setEarningsPerShare(CagrUtils.compounding(endYearAnnualData.getEarningsPerShare(),
				startYearAnnualData.getEarningsPerShare(), noOfYears));
		logger.info("CagrCalculationServiceImpl basicDetailsCalculation method -end");
		return cagrAnalyzedDetails;
	}

	@Override
	public CompoundedAnnualGrowthRate futureCashFlowCalculation(Map<String, Object> inputParams) {
		logger.info("CagrCalculationServiceImpl futureCashFlowCalculation method -start");
		CompoundedAnnualGrowthRate cagrAnalyzedDetails = (CompoundedAnnualGrowthRate) inputParams
				.get(CagrConstants.CAGR_ANALYZED_DETAILS);
		int analysisEndYear = (int) inputParams.get(CagrConstants.CAGR_ANALYSIS_END_YEAR);
		HistoricalDataWrapper futureCashFlowValue = new HistoricalDataWrapper();
		List<HistoricalData> futureCashFlows = new ArrayList<HistoricalData>();
		for (int futureYear = 0; futureYear <= 10; futureYear++) {
			HistoricalData futureCashFlow = new HistoricalData();
			futureCashFlow.setCurrentYear(analysisEndYear + futureYear);
			if (futureYear == 0) {
				futureCashFlow.setValue(cagrAnalyzedDetails.getAverageFreeCashFlow());
			} else if (futureYear < 6) {
				futureCashFlow
						.setValue(CagrUtils.futureCashFlowCalculation(futureCashFlows.get(futureYear - 1).getValue(),
								CagrConstants.FUTURE_CASH_FLOW_FIRST_FIVE_YEARS_INTEREST));
			} else {
				futureCashFlow
						.setValue(CagrUtils.futureCashFlowCalculation(futureCashFlows.get(futureYear - 1).getValue(),
								CagrConstants.FUTURE_CASH_FLOW_LAST_FIVE_YEARS_INTEREST));
			}
			futureCashFlows.add(futureCashFlow);
		}
		futureCashFlowValue.setHistoricalData(futureCashFlows);
		cagrAnalyzedDetails.setFutureCashFlowValue(futureCashFlowValue);
		cagrAnalyzedDetails.setTerminalValue(
				CagrUtils.terminalValueCalculation(futureCashFlows.get(futureCashFlows.size() - 1).getValue(),
						CagrConstants.TERMINAL_ANNUAL_INTEREST, CagrConstants.DISCOUNT_RATE));
		logger.info("CagrCalculationServiceImpl futureCashFlowCalculation method -End");
		return cagrAnalyzedDetails;
	}

	@SuppressWarnings("unchecked")
	@Override
	public CompoundedAnnualGrowthRate presentValueCalculation(Map<String, Object> inputParams) {
		logger.info("CagrCalculationServiceImpl presentValueCalculation method -start");
		CompoundedAnnualGrowthRate cagrAnalyzedDetails = (CompoundedAnnualGrowthRate) inputParams
				.get(CagrConstants.CAGR_ANALYZED_DETAILS);
		List<Annual> annuallyAnalyzedDetails = (List<Annual>) inputParams.get(CagrConstants.ANNUALLY_ANALYZED_DETAILS);
		Annual endYearAnnualDetails = annuallyAnalyzedDetails.get(annuallyAnalyzedDetails.size() - 1);
		HistoricalDataWrapper presentValueofFutureCashFlows = new HistoricalDataWrapper();
		List<HistoricalData> futureCashFlows = cagrAnalyzedDetails.getFutureCashFlowValue().getHistoricalData();
		List<HistoricalData> presentValueofFutureCashFlow = new ArrayList<HistoricalData>();
		for (int futureYear = 1; futureYear < futureCashFlows.size(); futureYear++) {
			HistoricalData presentCashValue = new HistoricalData();
			double presentValue = CagrUtils.presentValueCalculation(futureCashFlows.get(futureYear).getValue(),
					CagrConstants.DISCOUNT_RATE, futureYear);
			cagrAnalyzedDetails.setNetPresentValue(cagrAnalyzedDetails.getNetPresentValue() + presentValue);
			presentCashValue.setValue(presentValue);
			presentCashValue.setCurrentYear(futureYear);
			presentValueofFutureCashFlow.add(presentCashValue);
		}
		cagrAnalyzedDetails.setPresentFutureCashFlowValue(presentValueofFutureCashFlows);
		cagrAnalyzedDetails.setPresentValueOfTerminalValue(
				CagrUtils.presentValueCalculation(cagrAnalyzedDetails.getTerminalValue(), CagrConstants.DISCOUNT_RATE,
						futureCashFlows.get(futureCashFlows.size() - 1).getValue()));
		cagrAnalyzedDetails.setNetPresentValue(
				cagrAnalyzedDetails.getNetPresentValue() + cagrAnalyzedDetails.getPresentValueOfTerminalValue());
		cagrAnalyzedDetails.setNetDebt(
				endYearAnnualDetails.getTotalDebt() - endYearAnnualDetails.getCashAndCashEquivalentAtYearEnding());
		cagrAnalyzedDetails.setTotalPresentValueOfFreeFlowCash(
				cagrAnalyzedDetails.getNetPresentValue() - cagrAnalyzedDetails.getNetDebt());
		logger.info("CagrCalculationServiceImpl presentValueCalculation method -End");
		return cagrAnalyzedDetails;
	}

	@SuppressWarnings("unchecked")
	@Override
	public CompoundedAnnualGrowthRate sharePriceCalculation(Map<String, Object> inputParams) {
		logger.info("CagrCalculationServiceImpl sharePriceCalculation method -start");
		CompoundedAnnualGrowthRate cagrAnalyzedDetails = (CompoundedAnnualGrowthRate) inputParams
				.get(CagrConstants.CAGR_ANALYZED_DETAILS);
		List<Annual> annuallyAnalyzedDetails = (List<Annual>) inputParams.get(CagrConstants.ANNUALLY_ANALYZED_DETAILS);
		Annual endYearAnnualDetails = annuallyAnalyzedDetails.get(annuallyAnalyzedDetails.size() - 1);
		cagrAnalyzedDetails.setInternsicValueOfSharePrice(
				(cagrAnalyzedDetails.getTotalPresentValueOfFreeFlowCash() * CagrConstants.BASE_VALUE)
						/ endYearAnnualDetails.getTotalEquityShares());
		cagrAnalyzedDetails.setLowerInternsicValueOfSharePrice(
				cagrAnalyzedDetails.getInternsicValueOfSharePrice() * CagrConstants.LOWER_INTERNSIC_VALUE_FACTOR);
		cagrAnalyzedDetails.setUpperInternsicValueOfSharePrice(
				cagrAnalyzedDetails.getInternsicValueOfSharePrice() * CagrConstants.UPPER_INTERNSIC_VALUE_FACTOR);
		cagrAnalyzedDetails.setMarginOfSafety(
				cagrAnalyzedDetails.getInternsicValueOfSharePrice() * CagrConstants.MARGIN_OF_SAFETY_FACTOR);
		logger.info("CagrCalculationServiceImpl sharePriceCalculation method -End");
		return cagrAnalyzedDetails;
	}

	@SuppressWarnings("unchecked")
	@Override
	public CompoundedAnnualGrowthRate growthAndHistoricalDataCalculation(Map<String, Object> inputParams) {
		logger.info("CagrCalculationServiceImpl componentsGrowthCalculation method -start");
		CompoundedAnnualGrowthRate cagrAnalyzedDetails = (CompoundedAnnualGrowthRate) inputParams
				.get(CagrConstants.CAGR_ANALYZED_DETAILS);
		List<Annual> annuallyAnalyzedDetails = (List<Annual>) inputParams.get(CagrConstants.ANNUALLY_ANALYZED_DETAILS);
		HistoricalDataWrapper historicalTotalRevenue = new HistoricalDataWrapper();
		HistoricalDataWrapper totalRevenueGrowth = new HistoricalDataWrapper();
		HistoricalDataWrapper historicalProfitAfterTax = new HistoricalDataWrapper();
		HistoricalDataWrapper historicalProfitAfterTaxMargin = new HistoricalDataWrapper();
		HistoricalDataWrapper profitAfterTaxGrowth = new HistoricalDataWrapper();
		HistoricalDataWrapper historicalSalesPerShare = new HistoricalDataWrapper();
		HistoricalDataWrapper salesPerShareGrowth = new HistoricalDataWrapper();
		HistoricalDataWrapper historicalBookValuePerShare = new HistoricalDataWrapper();
		HistoricalDataWrapper bookValuePerShareGrowth = new HistoricalDataWrapper();
		HistoricalDataWrapper historicalEarningsPerShare = new HistoricalDataWrapper();
		HistoricalDataWrapper earningsPerShareGrowth = new HistoricalDataWrapper();
		HistoricalDataWrapper historicalGrossProfit = new HistoricalDataWrapper();
		HistoricalDataWrapper historicalGrossProfitMargin = new HistoricalDataWrapper();
		HistoricalDataWrapper grossProfitGrowth = new HistoricalDataWrapper();
		HistoricalDataWrapper historicalTotalDebt = new HistoricalDataWrapper();
		HistoricalDataWrapper historicalEarningsBfrTax = new HistoricalDataWrapper();
		HistoricalDataWrapper historicalTotalDebtToEarningsBfrTax = new HistoricalDataWrapper();
		HistoricalDataWrapper historicalInventory = new HistoricalDataWrapper();
		HistoricalDataWrapper inventoryGrowth = new HistoricalDataWrapper();
		HistoricalDataWrapper historicalInventoryNoOfDays = new HistoricalDataWrapper();
		HistoricalDataWrapper historicalNetSales = new HistoricalDataWrapper();
		HistoricalDataWrapper netSalesGrowth = new HistoricalDataWrapper();
		HistoricalDataWrapper historicalTradeReceivables = new HistoricalDataWrapper();
		HistoricalDataWrapper historicalTradeReceivablesByNetSales = new HistoricalDataWrapper();
		HistoricalDataWrapper historicalCashFlowFromOperations = new HistoricalDataWrapper();
		HistoricalDataWrapper cashFlowFromOperationsGrowth = new HistoricalDataWrapper();
		HistoricalDataWrapper historicalCashAndCashEquivalent = new HistoricalDataWrapper();
		HistoricalDataWrapper cashAndCashEquivalentGrowth = new HistoricalDataWrapper();
		HistoricalDataWrapper historicalShareHoldersFund = new HistoricalDataWrapper();
		HistoricalDataWrapper shareHoldersFundGrowth = new HistoricalDataWrapper();
		HistoricalDataWrapper historicalReturnOnEquity = new HistoricalDataWrapper();
		HistoricalDataWrapper historicalOperatingRevenue = new HistoricalDataWrapper();
		HistoricalDataWrapper operatingRevenueGrowth = new HistoricalDataWrapper();
		HistoricalDataWrapper historicalSharePrice = new HistoricalDataWrapper();
		HistoricalDataWrapper sharePriceGrowth = new HistoricalDataWrapper();
		List<HistoricalData> historicalTotalRevenueList = new ArrayList<HistoricalData>();
		List<HistoricalData> totalRevenueGrowthList = new ArrayList<HistoricalData>();
		List<HistoricalData> historicalProfitAfterTaxList = new ArrayList<HistoricalData>();
		List<HistoricalData> historicalProfitAfterTaxMarginList = new ArrayList<HistoricalData>();
		List<HistoricalData> profitAfterTaxGrowthList = new ArrayList<HistoricalData>();
		List<HistoricalData> historicalSalesPerShareList = new ArrayList<HistoricalData>();
		List<HistoricalData> salesPerShareGrowthList = new ArrayList<HistoricalData>();
		List<HistoricalData> historicalBookValuePerShareList = new ArrayList<HistoricalData>();
		List<HistoricalData> bookValuePerShareGrowthList = new ArrayList<HistoricalData>();
		List<HistoricalData> historicalEarningsPerShareList = new ArrayList<HistoricalData>();
		List<HistoricalData> earningsPerShareGrowthList = new ArrayList<HistoricalData>();
		List<HistoricalData> historicalGrossProfitList = new ArrayList<HistoricalData>();
		List<HistoricalData> historicalGrossProfitMarginList = new ArrayList<HistoricalData>();
		List<HistoricalData> grossProfitGrowthList = new ArrayList<HistoricalData>();
		List<HistoricalData> historicalTotalDebtList = new ArrayList<HistoricalData>();
		List<HistoricalData> historicalEarningsBfrTaxList = new ArrayList<HistoricalData>();
		List<HistoricalData> historicalTotalDebtToEarningsBfrTaxList = new ArrayList<HistoricalData>();
		List<HistoricalData> historicalInventoryList = new ArrayList<HistoricalData>();
		List<HistoricalData> inventoryGrowthList = new ArrayList<HistoricalData>();
		List<HistoricalData> historicalInventoryNoOfDaysList = new ArrayList<HistoricalData>();
		List<HistoricalData> historicalNetSalesList = new ArrayList<HistoricalData>();
		List<HistoricalData> netSalesGrowthList = new ArrayList<HistoricalData>();
		List<HistoricalData> historicalTradeReceivablesList = new ArrayList<HistoricalData>();
		List<HistoricalData> historicalTradeReceivablesByNetSalesList = new ArrayList<HistoricalData>();
		List<HistoricalData> historicalCashFlowFromOperationsList = new ArrayList<HistoricalData>();
		List<HistoricalData> cashFlowFromOperationsGrowthList = new ArrayList<HistoricalData>();
		List<HistoricalData> historicalCashAndCashEquivalentList = new ArrayList<HistoricalData>();
		List<HistoricalData> cashAndCashEquivalentGrowthList = new ArrayList<HistoricalData>();
		List<HistoricalData> historicalShareHoldersFundList = new ArrayList<HistoricalData>();
		List<HistoricalData> shareHoldersFundGrowthList = new ArrayList<HistoricalData>();
		List<HistoricalData> historicalReturnOnEquityList = new ArrayList<HistoricalData>();
		List<HistoricalData> historicalOperatingRevenueList = new ArrayList<HistoricalData>();
		List<HistoricalData> operatingRevenueGrowthList = new ArrayList<HistoricalData>();
		List<HistoricalData> historicalSharePriceList = new ArrayList<HistoricalData>();
		List<HistoricalData> sharePriceGrowthList = new ArrayList<HistoricalData>();

		for (Annual annual : annuallyAnalyzedDetails) {
			int index = annuallyAnalyzedDetails.indexOf(annual);
			HistoricalData historicalData = new HistoricalData();
			historicalData.setCurrentYear(annual.getAnalysisYear());

			historicalData.setValue(annual.getTotalRevenue());
			historicalTotalRevenueList.add(historicalData);

			historicalData.setValue(annual.getProfitAfterTax());
			historicalProfitAfterTaxList.add(historicalData);

			historicalData.setValue(annual.getProfitAfterTaxMargin());
			historicalProfitAfterTaxMarginList.add(historicalData);

			historicalData.setValue(annual.getSalesPerShare());
			historicalSalesPerShareList.add(historicalData);

			historicalData.setValue(annual.getBookValuePerShare());
			historicalBookValuePerShareList.add(historicalData);

			historicalData.setValue(annual.getEarningsPerShare());
			historicalEarningsPerShareList.add(historicalData);

			historicalData.setValue(annual.getGrossProfit());
			historicalGrossProfitList.add(historicalData);

			historicalData.setValue(annual.getGrossProfitMargin());
			historicalGrossProfitMarginList.add(historicalData);

			historicalData.setValue(annual.getTotalDebt());
			historicalTotalDebtList.add(historicalData);

			historicalData.setValue(annual.getEarningsBeforeIncomeTax());
			historicalEarningsBfrTaxList.add(historicalData);

			historicalData.setValue((annual.getTotalDebt() / annual.getEarningsBeforeIncomeTax()) * 100);
			historicalTotalDebtToEarningsBfrTaxList.add(historicalData);

			historicalData.setValue(annual.getInventory());
			historicalInventoryList.add(historicalData);

			historicalData.setValue(annual.getInventoryNoOfDays());
			historicalInventoryNoOfDaysList.add(historicalData);

			historicalData.setValue(annual.getNetSales());
			historicalNetSalesList.add(historicalData);

			historicalData.setValue(annual.getTradeReceivables());
			historicalTradeReceivablesList.add(historicalData);

			historicalData.setValue(annual.getTradeReceivables() / annual.getNetSales());
			historicalTradeReceivablesByNetSalesList.add(historicalData);

			historicalData.setValue(annual.getCashFlowFromOperatingActivities());
			historicalCashFlowFromOperationsList.add(historicalData);

			historicalData.setValue(annual.getCashAndCashEquivalentAtYearEnding());
			historicalCashAndCashEquivalentList.add(historicalData);

			historicalData.setValue(annual.getShareHoldersFundAndTotalEquity());
			historicalShareHoldersFundList.add(historicalData);

			historicalData.setValue(annual.getReturnOnEquity());
			historicalReturnOnEquityList.add(historicalData);

			historicalData.setValue(annual.getOperatingRevenue());
			historicalOperatingRevenueList.add(historicalData);

			historicalData.setValue(annual.getSharePrice());
			historicalSharePriceList.add(historicalData);

			if (index != 0) {
				historicalData = new HistoricalData();
				historicalData.setCurrentYear(annual.getAnalysisYear());
				historicalData.setPriorYear(annuallyAnalyzedDetails.get(index - 1).getAnalysisYear());

				historicalData.setValue(CagrUtils.growthCalculation(annual.getTotalRevenue(),
						annuallyAnalyzedDetails.get(index - 1).getTotalRevenue()));
				totalRevenueGrowthList.add(historicalData);

				historicalData.setValue(CagrUtils.growthCalculation(annual.getProfitAfterTax(),
						annuallyAnalyzedDetails.get(index - 1).getProfitAfterTax()));
				profitAfterTaxGrowthList.add(historicalData);

				historicalData.setValue(CagrUtils.growthCalculation(annual.getSalesPerShare(),
						annuallyAnalyzedDetails.get(index - 1).getSalesPerShare()));
				salesPerShareGrowthList.add(historicalData);

				historicalData.setValue(CagrUtils.growthCalculation(annual.getBookValuePerShare(),
						annuallyAnalyzedDetails.get(index - 1).getBookValuePerShare()));
				bookValuePerShareGrowthList.add(historicalData);

				historicalData.setValue(CagrUtils.growthCalculation(annual.getEarningsPerShare(),
						annuallyAnalyzedDetails.get(index - 1).getEarningsPerShare()));
				earningsPerShareGrowthList.add(historicalData);

				historicalData.setValue(CagrUtils.growthCalculation(annual.getGrossProfit(),
						annuallyAnalyzedDetails.get(index - 1).getGrossProfit()));
				grossProfitGrowthList.add(historicalData);

				historicalData.setValue(CagrUtils.growthCalculation(annual.getInventory(),
						annuallyAnalyzedDetails.get(index - 1).getInventory()));
				inventoryGrowthList.add(historicalData);

				historicalData.setValue(CagrUtils.growthCalculation(annual.getNetSales(),
						annuallyAnalyzedDetails.get(index - 1).getNetSales()));
				netSalesGrowthList.add(historicalData);

				historicalData.setValue(CagrUtils.growthCalculation(annual.getCashFlowFromOperatingActivities(),
						annuallyAnalyzedDetails.get(index - 1).getCashFlowFromOperatingActivities()));
				cashFlowFromOperationsGrowthList.add(historicalData);

				historicalData.setValue(CagrUtils.growthCalculation(annual.getCashAndCashEquivalentAtYearEnding(),
						annuallyAnalyzedDetails.get(index - 1).getCashAndCashEquivalentAtYearEnding()));
				cashAndCashEquivalentGrowthList.add(historicalData);

				historicalData.setValue(CagrUtils.growthCalculation(annual.getShareHoldersFundAndTotalEquity(),
						annuallyAnalyzedDetails.get(index - 1).getShareHoldersFundAndTotalEquity()));
				shareHoldersFundGrowthList.add(historicalData);

				historicalData.setValue(CagrUtils.growthCalculation(annual.getOperatingRevenue(),
						annuallyAnalyzedDetails.get(index - 1).getOperatingRevenue()));
				operatingRevenueGrowthList.add(historicalData);

				historicalData.setValue(CagrUtils.growthCalculation(annual.getSharePrice(),
						annuallyAnalyzedDetails.get(index - 1).getSharePrice()));
				sharePriceGrowthList.add(historicalData);
			}
		}
		historicalTotalRevenue.setHistoricalData(historicalTotalRevenueList);
		totalRevenueGrowth.setHistoricalData(totalRevenueGrowthList);
		historicalProfitAfterTax.setHistoricalData(historicalProfitAfterTaxList);
		historicalProfitAfterTaxMargin.setHistoricalData(historicalProfitAfterTaxMarginList);
		profitAfterTaxGrowth.setHistoricalData(profitAfterTaxGrowthList);
		historicalSalesPerShare.setHistoricalData(historicalSalesPerShareList);
		salesPerShareGrowth.setHistoricalData(salesPerShareGrowthList);
		historicalBookValuePerShare.setHistoricalData(historicalBookValuePerShareList);
		bookValuePerShareGrowth.setHistoricalData(bookValuePerShareGrowthList);
		historicalEarningsPerShare.setHistoricalData(historicalEarningsPerShareList);
		earningsPerShareGrowth.setHistoricalData(earningsPerShareGrowthList);
		historicalGrossProfit.setHistoricalData(historicalGrossProfitList);
		historicalGrossProfitMargin.setHistoricalData(historicalGrossProfitMarginList);
		grossProfitGrowth.setHistoricalData(grossProfitGrowthList);
		historicalTotalDebt.setHistoricalData(historicalTotalDebtList);
		historicalEarningsBfrTax.setHistoricalData(historicalEarningsBfrTaxList);
		historicalTotalDebtToEarningsBfrTax.setHistoricalData(historicalTotalDebtToEarningsBfrTaxList);
		historicalInventory.setHistoricalData(historicalInventoryList);
		inventoryGrowth.setHistoricalData(inventoryGrowthList);
		historicalInventoryNoOfDays.setHistoricalData(historicalInventoryNoOfDaysList);
		historicalNetSales.setHistoricalData(historicalNetSalesList);
		netSalesGrowth.setHistoricalData(netSalesGrowthList);
		historicalTradeReceivables.setHistoricalData(historicalTradeReceivablesList);
		historicalTradeReceivablesByNetSales.setHistoricalData(historicalTradeReceivablesByNetSalesList);
		historicalCashFlowFromOperations.setHistoricalData(historicalCashFlowFromOperationsList);
		cashFlowFromOperationsGrowth.setHistoricalData(cashFlowFromOperationsGrowthList);
		historicalCashAndCashEquivalent.setHistoricalData(historicalCashAndCashEquivalentList);
		cashAndCashEquivalentGrowth.setHistoricalData(cashAndCashEquivalentGrowthList);
		historicalShareHoldersFund.setHistoricalData(historicalShareHoldersFundList);
		shareHoldersFundGrowth.setHistoricalData(shareHoldersFundGrowthList);
		historicalReturnOnEquity.setHistoricalData(historicalReturnOnEquityList);
		historicalOperatingRevenue.setHistoricalData(historicalOperatingRevenueList);
		operatingRevenueGrowth.setHistoricalData(operatingRevenueGrowthList);
		historicalSharePrice.setHistoricalData(historicalSharePriceList);
		sharePriceGrowth.setHistoricalData(sharePriceGrowthList);
		cagrAnalyzedDetails.setHistoricalTotalRevenue(historicalTotalRevenue);
		cagrAnalyzedDetails.setTotalRevenueGrowth(totalRevenueGrowth);
		cagrAnalyzedDetails.setHistoricalProfitAfterTax(historicalProfitAfterTax);
		cagrAnalyzedDetails.setHistoricalProfitAfterTaxMargin(historicalProfitAfterTaxMargin);
		cagrAnalyzedDetails.setProfitAfterTaxGrowth(profitAfterTaxGrowth);
		cagrAnalyzedDetails.setHistoricalSalesPerShare(historicalSalesPerShare);
		cagrAnalyzedDetails.setSalesPerShareGrowth(salesPerShareGrowth);
		cagrAnalyzedDetails.setHistoricalBookValuePerShare(historicalBookValuePerShare);
		cagrAnalyzedDetails.setBookValuePerShareGrowth(bookValuePerShareGrowth);
		cagrAnalyzedDetails.setHistoricalEarningsPerShare(historicalEarningsPerShare);
		cagrAnalyzedDetails.setEarningsPerShareGrowth(earningsPerShareGrowth);
		cagrAnalyzedDetails.setHistoricalGrossProfit(historicalGrossProfit);
		cagrAnalyzedDetails.setHistoricalGrossProfitMargin(historicalGrossProfitMargin);
		cagrAnalyzedDetails.setGrossProfitGrowth(grossProfitGrowth);
		cagrAnalyzedDetails.setHistoricalTotalDebt(historicalTotalDebt);
		cagrAnalyzedDetails.setHistoricalEarningsBeforeIncTax(historicalEarningsBfrTax);
		cagrAnalyzedDetails.setHistoricalTotalDebtByEarningsBfrIncTax(historicalTotalDebtToEarningsBfrTax);
		cagrAnalyzedDetails.setHistoricalInventory(historicalInventory);
		cagrAnalyzedDetails.setInventoryGrowth(inventoryGrowth);
		cagrAnalyzedDetails.setHistoricalInventoryNoOfDays(historicalInventoryNoOfDays);
		cagrAnalyzedDetails.setHistoricalNetSales(historicalNetSales);
		cagrAnalyzedDetails.setNetSalesGrowth(netSalesGrowth);
		cagrAnalyzedDetails.setHistoricalTradeReceivables(historicalTradeReceivables);
		cagrAnalyzedDetails.setHistoricalTradeRecievablesByNetSales(historicalTradeReceivablesByNetSales);
		cagrAnalyzedDetails.setHistoricalCashFlowFromOperations(historicalCashFlowFromOperations);
		cagrAnalyzedDetails.setCashFlowFromOperationsGrowth(cashFlowFromOperationsGrowth);
		cagrAnalyzedDetails.setHistoricalCashAndCashEquivalent(historicalCashAndCashEquivalent);
		cagrAnalyzedDetails.setCashAndCashEquivalentGrowth(cashAndCashEquivalentGrowth);
		cagrAnalyzedDetails.setHistoricalShareholdersFund(historicalShareHoldersFund);
		cagrAnalyzedDetails.setShareholdersFundGrowth(shareHoldersFundGrowth);
		cagrAnalyzedDetails.setHistoricalReturnOnEquity(historicalReturnOnEquity);
		cagrAnalyzedDetails.setHistoricalOperatingRevenue(historicalOperatingRevenue);
		cagrAnalyzedDetails.setOperatingRevenueGrowth(operatingRevenueGrowth);
		cagrAnalyzedDetails.setHistoricalSharePrice(historicalSharePrice);
		cagrAnalyzedDetails.setSharePriceGrowth(sharePriceGrowth);
		logger.info("CagrCalculationServiceImpl componentsGrowthCalculation method -End");
		return cagrAnalyzedDetails;
	}

	@Override
	public CompoundedAnnualGrowthRate crucialDetailsCheckCalculation(Map<String, Object> inputParams) {
		logger.info("CagrCalculationServiceImpl crucialDetailsCheckCalculation method -start");
		CompoundedAnnualGrowthRate cagrAnalyzedDetails = (CompoundedAnnualGrowthRate) inputParams
				.get(CagrConstants.CAGR_ANALYZED_DETAILS);
		cagrAnalyzedDetails.setTotalRevenueInLineWithProfitAfterTaxCheck(CagrUtils.totalRevenueInLineWithPATCheck(
				cagrAnalyzedDetails.getTotalRevenueGrowth().getHistoricalData(),
				cagrAnalyzedDetails.getProfitAfterTaxGrowth().getHistoricalData()));
		cagrAnalyzedDetails.setEarningsPerShareGrowthInLineWithProfitAfterTaxCheck(CagrUtils
				.earningsPerShareInLineWithPATCheck(cagrAnalyzedDetails.getEarningsPerShareGrowth().getHistoricalData(),
						cagrAnalyzedDetails.getProfitAfterTaxGrowth().getHistoricalData()));
		cagrAnalyzedDetails.setGrossProfitMarginCheck(CagrUtils
				.grossProfitMariginCheck(cagrAnalyzedDetails.getHistoricalGrossProfitMargin().getHistoricalData()));
		cagrAnalyzedDetails.setDebtLevelCheck(CagrUtils
				.debtLevelCheck(cagrAnalyzedDetails.getHistoricalTotalDebtByEarningsBfrIncTax().getHistoricalData()));
		cagrAnalyzedDetails.setInventoryGrowthInLineWithProfitAfterTaxCheck(
				CagrUtils.inventoryInLineWithPATCheck(cagrAnalyzedDetails.getInventoryGrowth().getHistoricalData(),
						cagrAnalyzedDetails.getProfitAfterTaxGrowth().getHistoricalData()));
		cagrAnalyzedDetails.setTradeReceivablesToNetSalesCheck(CagrUtils.tradeRecievablesToNetSalesCheck(
				cagrAnalyzedDetails.getHistoricalTradeRecievablesByNetSales().getHistoricalData()));
		cagrAnalyzedDetails.setCashFlowFromOperationsCheck(CagrUtils.cashFlowFromOperatingActivitiesCheck(
				cagrAnalyzedDetails.getHistoricalCashFlowFromOperations().getHistoricalData()));
		cagrAnalyzedDetails.setReturnOnEquityCheck(
				CagrUtils.returnOnEquityCheck(cagrAnalyzedDetails.getHistoricalReturnOnEquity().getHistoricalData()));
		logger.info("CagrCalculationServiceImpl crucialDetailsCheckCalculation method -End");
		return cagrAnalyzedDetails;
	}

}
