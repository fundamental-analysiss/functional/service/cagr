package com.naga.share.market.fundamental.analysis.services.cagr;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.naga.share.market.fundamental.analysis.model.cagr.entity.CompoundedAnnualGrowthRate;

/**
 * @author Nagaaswin S
 *
 */
@Service
public interface CagrCalculationService {

	/**
	 * 
	 * Service method to calculate CAGR for a company
	 * 
	 * @param inputParams
	 * @return CompoundedAnnualGrowthRate
	 */
	public CompoundedAnnualGrowthRate cagrCalculation(Map<String, Object> inputParams);

	/**
	 * 
	 * Service method to calculate CAGR basic details for a company
	 * 
	 * @param inputParams
	 * @return CompoundedAnnualGrowthRate
	 */
	public CompoundedAnnualGrowthRate basicDetailsCalculation(Map<String, Object> inputParams);

	/**
	 * 
	 * Service method to calculate future cash flow for a company
	 * 
	 * @param inputParams
	 * @return CompoundedAnnualGrowthRate
	 */
	public CompoundedAnnualGrowthRate futureCashFlowCalculation(Map<String, Object> inputParams);

	/**
	 * 
	 * Service method to calculate present value for a company
	 * 
	 * @param inputParams
	 * @return CompoundedAnnualGrowthRate
	 */
	public CompoundedAnnualGrowthRate presentValueCalculation(Map<String, Object> inputParams);

	/**
	 * 
	 * Service method to calculate share price for a company
	 * 
	 * @param inputParams
	 * @return CompoundedAnnualGrowthRate
	 */
	public CompoundedAnnualGrowthRate sharePriceCalculation(Map<String, Object> inputParams);

	/**
	 * 
	 * Service method to calculate CAGR growth for a company
	 * 
	 * @param inputParams
	 * @return CompoundedAnnualGrowthRate
	 */
	public CompoundedAnnualGrowthRate growthAndHistoricalDataCalculation(Map<String, Object> inputParams);

	/**
	 * 
	 * Service method to check CAGR details for a company
	 * 
	 * @param inputParams
	 * @return CompoundedAnnualGrowthRate
	 */
	public CompoundedAnnualGrowthRate crucialDetailsCheckCalculation(Map<String, Object> inputParams);
}
